#!/usr/bin/env python

edad = int(input('Digite su edad: '))
"""se le agrega int por que si le asignamos a edad lo que se digita quedara
solo como string. se agrega int para que al digitar se convierta
en int el string"""

if edad < 0:
    print("error no puede ser negativa.. bueno si eres chuck norris si puedes")
elif edad < 18:
    print("eres menor de edad")
else:
    print("eres mayor de edad")
