#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess

dato = input(u'Introduce Operación: ')

resultado = 0

serie = dato.split('+')
if len(serie) == 2:
    os.system("espeak -v mb-es2 'Lo que usted piensa realizar es una suma' -s 160 2>/dev/null ")
    os.system("espeak -v mb-es2 'el resultado de la suma es: ' -s 160 2>/dev/null")
    resultado = int(serie[0]) + int(serie[1])
else:
    serie = dato.split('-')
    if len(serie) == 2:
        os.system("espeak -v mb-es2 'Lo que usted piensa realizar es una resta' -s 160 2>/dev/null")
        os.system("espeak -v mb-es2 'el resultado de la resta es: ' -s 160 2>/dev/null")
        resultado = int(serie[0]) - int(serie[1])
    else:
        serie = dato.split('*')
        if len(serie) == 2:
            os.system("espeak -v mb-es2 'Lo que usted piensa realizar es una multiplicación' -s 160 2>/dev/null ")
            os.system("espeak -v mb-es2 'el resultado de la multiplicación es: ' -s 160 2>/dev/null")
            resultado = int(serie[0]) * int(serie[1])
        else:
            serie = dato.split('/')
            if len(serie) == 2:
                os.system("espeak -v mb-es2 'Lo que usted piensa realizar es una división' -s 160 2>/dev/null ")
                os.system("espeak -v mb-es2 'el resultado de la división es: ' -s 160 2>/dev/null")
                resultado = int(serie[0]) / int(serie[1])

result = str(resultado)
print("El resultado es: ", resultado)
fnull = open(os.devnull, 'w')
subprocess.Popen(["espeak", "-v", "mb-es2", "-s 160", result], stdout=fnull, stderr=subprocess.STDOUT)
